public class Player extends Character{

    public int nAttackkUpgrades, nDefendUpgrades;

    int zlataky, pocetOdpocinku, potak;

    public String[] attackUpgrades = {"Strength", "Power", "God-Strength"};
    public String[] defendUpgrades = {"Těžké kosti", "Basic Armor", "God-Mode"};

    public Player(String name) {
        super(name, 100, 0);
        this.nAttackkUpgrades = 0;
        this.nDefendUpgrades = 0;
        this.zlataky = 5;
        this.pocetOdpocinku = 1;
        this.potak = 0;
        vyberVlastnost();
    }

    public int attack() {
        return (int) (Math.random()*(XP/4 + nAttackkUpgrades*3 + 3) + XP/10 + nAttackkUpgrades*2 + nDefendUpgrades + 1);
    }

    public int defend() {
        return (int) (Math.random()*(XP/4 + nDefendUpgrades*3 + 3) + XP/10 + nDefendUpgrades*2 + nAttackkUpgrades + 1);
    }

    //hráč si vybírá vlastnosti (skill pointy)
    public void vyberVlastnost(){
        GameLogic.vycistiKonzoli();
        GameLogic.vypisNazev("Vyber si vlastnost:");
        if (nAttackkUpgrades == 3){ // pokud se hráč rozhodl jít full-attack (došli mu attack upgrady), automaticky se mu přiřadí defend vlastnost.
            System.out.println("Už máš všechny vylepšení na útok. Automaticky jsem ti přidělil vylepšení obrany (" + defendUpgrades[nDefendUpgrades] + ").");
            nDefendUpgrades++;
        } else if (nDefendUpgrades == 3){ // pokud se hráč rozhodl jít full-defend (došli mu defend upgrady), automaticky se mu přiřadí attack vlastnost.
            System.out.println("Už máš všechny vylepšení na obranu. Automaticky jsem ti přidělil vylepšení útoku (" + attackUpgrades[nAttackkUpgrades] + ").");
            nAttackkUpgrades++;
        } else { // pokud hráč nešel full-attack ani full-defend, má normální výběr
            System.out.println("(1) " + attackUpgrades[nAttackkUpgrades]);
            System.out.println("(2) " + defendUpgrades[nDefendUpgrades]);
            int input = GameLogic.prectiInt("->", 2);
            GameLogic.vycistiKonzoli();
            if (input == 1){
                GameLogic.vypisNazev("Vybral(a) jsi si " + attackUpgrades[nAttackkUpgrades] + ".");
                nAttackkUpgrades++;
            } else {
                GameLogic.vypisNazev("Vybral(a) jsi si " + defendUpgrades[nDefendUpgrades] + ".");
                nDefendUpgrades++;
            }
        }
        GameLogic.cekejNaReakci();
    }
}