public class Pribeh {
    public static void vypisIntro(){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("Příběh");
        GameLogic.lajnaLajn(30);
        System.out.println("Jsi poslední přeživší své vesnice, která byla zničena poskoky krutého vládce jménem Gabriel Payne.");
        System.out.println("Všichni tvoji blízcí a lidi, které jsi měl rád, jsou mrtvý a rozsekaný na milion kousků.");
        System.out.println("Chceš jenom odplatu... víš, kam tím mířím? Je čas na omertu.");
        GameLogic.cekejNaReakci();
    }

    public static void vypisIntroPrvnihoAktu(){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("intro prvního aktu");
        GameLogic.lajnaLajn(30);
        System.out.println("Na začátek své dlouhé cesty se vydáte do blízských lesů. Jenom přes tento les se dostanete do Večných hor");
        System.out.println("Věcné hory je velmi nebezpečné místo. Spousta dobrodruhů, co se tam vydalo, zůstalo už na věcnost.");
        System.out.println("");
        GameLogic.cekejNaReakci();
    }

    public static void vypisOutroPrvnihoAktu(){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("outro prvního aktu");
        GameLogic.lajnaLajn(30);
        System.out.println("Po dlouhém dni jste se konečně dostali k horám.");
        GameLogic.cekejNaReakci();
    }

    public static void vypisIntroDruhehoAktu(){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("intro druhého aktu");
        GameLogic.lajnaLajn(30);
        System.out.println();
        System.out.println("Je ti jedno, co jste slyšeli o tomto krutém místě, pokud je toto jediná cesta, jak dokončit omertu.");
        GameLogic.cekejNaReakci();
    }

    public static void vypisOutroDruhehoAktu(){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("outro druhého aktu");
        GameLogic.lajnaLajn(30);
        System.out.println("Dokázal jsi to! Překročil jsi Věcné hory a jsi stále na živu!");
        System.out.println("Když slezeš posledních pár metrů posledního kopce, jsi rád, že cítíš tu krásnou pevnou zem pod nohama.");
        System.out.println("\nCítíš se silnější a ta zkušenosti ti umožnuje vylepšit si jednu vlastnost.");
        GameLogic.cekejNaReakci();
    }

    public static void vypisIntroTretihoAktu(){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("intro třetího aktu");
        GameLogic.lajnaLajn(30);
        System.out.println("Začnete cestovat na té pevné zemi.");
        System.out.println("Jdete kolem míst, která byla kdysi velmi obydlena.");
        System.out.println("Slyšíš pouze cinkání zlaťáků, které jsi pozbýral z příšer, které jsi zabil.");
        System.out.println("Doufáš, že se na tvé cestě objeví nějaký živý obchodník.");
        System.out.println("Díky prvním montrům víš, kde je sídlo krutého vládce, ale cesta je ještě velmi dlouhá.");
        GameLogic.cekejNaReakci();
    }

    public static void vypisOutroTretihoAktu(){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("outro třetího aktu");
        GameLogic.lajnaLajn(30);
        System.out.println("Začínáš mě překvapovat. Myslel jsem si, že se z toho ticha zblázníš, nebo tě někde zabije nějaký poskok.");
        System.out.println("Už jsi jenom pár hodin od tvé finální destinace. Hrad krutého vládce Gabriela Paynea.");
        System.out.println("Měl by jsi si dopřát odpočinku předtím, než tam dojdeš.");
        System.out.println("\nVždyť jsi taky získal spoustu zkušeností a můžeš si zvolit další vlastnost.");
        GameLogic.cekejNaReakci();
    }

    public static void vypisIntroCtvrtehoAktu(){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("intro třetího aktu");
        GameLogic.lajnaLajn(30);
        System.out.println("bitva před hradem");
        GameLogic.cekejNaReakci();
    }


    public static void vypisOutroCtvrtehoAktu(){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("entry vládcovo sálu");
        GameLogic.lajnaLajn(30);
        System.out.println("");
        GameLogic.cekejNaReakci();
    }

    public static void IntroBossFigtu(){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("Pátý AKT - intro");
        GameLogic.lajnaLajn(30);
        System.out.println("nějaký popis");
        GameLogic.cekejNaReakci();
    }

    public static void vypisKonec(Player player){
        GameLogic.vycistiKonzoli();
        GameLogic.lajnaLajn(30);
        System.out.println("");
        GameLogic.lajnaLajn(30);
        System.out.println("");
    }
}
