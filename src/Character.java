public abstract class Character {
    public String name;
    public int maxHP, HP, XP;
    public Character(String name, int maxHP, int XP){
        this.name = name;
        this.maxHP = maxHP;
        this.XP = XP;
        this.HP = maxHP;
    }

    public abstract int attack();
    public abstract int defend();
}

