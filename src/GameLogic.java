import java.util.Random;
import java.util.Scanner;

public class GameLogic {
    static Scanner scanner = new Scanner(System.in);

    static Player player;

    public static boolean beziJeste;

    public static int misto = 0, akt = 1;
    public static String[]mista = {"Obrovský les", "Věčné hory", "Divné Pláně", "Před hradem", "Vládcův sál"};

    public static String[] nahodneUdalosti = {"Souboj", "Souboj", "Souboj", "Odpočinek", "Odpočinek"};

    public static String[] enemies = {"Giant Spider", "Dragon", "Bandit", "Giant", "Bear", "Vampire", "Werewolf", "Witch"};
    public static int prectiInt(String prompt, int userChoices){
        int input;

        do{
            System.out.println(prompt);
            try{
                input = Integer.parseInt(scanner.next());
            }catch(Exception e){
                input = -1;
                System.out.println("Vložte číslo");
            }
        }while(input < 1 || input > userChoices);
        return input;
    }

    public static void vycistiKonzoli(){
        for(int i = 0; i < 100; i++)
            System.out.println();
    }

    public static void lajnaLajn(int n){
        for(int i = 0; i < n; i++)
            System.out.print("-");
        System.out.println();
    }

    public static void vypisNazev(String title){
        lajnaLajn(30);
        System.out.println(title);
        lajnaLajn(30);
    }

    public static void cekejNaReakci(){
        System.out.println("\nMůžeme pokračovat?");
        scanner.next();
    }

    public static void zacatekHry() throws InterruptedException {
        boolean nameSet = false;
        String name;
        vycistiKonzoli();
        lajnaLajn(40);
        lajnaLajn(30);
        System.out.println("Moje RPGčko");
        lajnaLajn(30);
        lajnaLajn(40);
        cekejNaReakci();

        do {
            vycistiKonzoli();
            vypisNazev("Jaké je tvé jméno?");
            name = scanner.next();
            // možná změna jména
            vycistiKonzoli();
            vypisNazev("Tvoje jméno je " + name + ".\nJe to správně?");
            System.out.println("(1) Ano");
            System.out.println("(2) Ne, chci se přejmenovat.");
            int input = prectiInt("-> ", 2);
            if (input == 1)
                nameSet = true;
        } while (!nameSet);

        player = new Player(name);

        beziJeste = true;

        herniLoop();
    }
        public static void zkontrolujAkt() throws InterruptedException {
            if (player.XP >= 10 && akt == 1) { //nastaví hráče do prvního aktu
                akt = 2;
                misto = 1;
                Pribeh.vypisOutroPrvnihoAktu();
                player.vyberVlastnost(); //level up
                Pribeh.vypisIntroDruhehoAktu();
                enemies[0] = "Velký rozzlobený pavouk";
                enemies[1] = "Bezzubka";
                enemies[2] = "Robin Hood";
                enemies[3] = "Obr jménem Karel";
                enemies[4] = "Méďa brumík";
                enemies[5] = "Drákula";
                enemies[6] = "Vlkodlak Vikatis";
                enemies[7] = "Gargamel";
                nahodneUdalosti[0] = "Souboj";
                nahodneUdalosti[1] = "Souboj";
                nahodneUdalosti[2] = "Souboj";
                nahodneUdalosti[3] = "Souboj";
                nahodneUdalosti[4] = "Obchodník";
            } else if (player.XP >= 25 && akt == 2) {
                akt = 3;
                misto = 2;
                Pribeh.vypisOutroDruhehoAktu();
                player.vyberVlastnost();
                Pribeh.vypisIntroTretihoAktu();
            } else if (player.XP >= 50 && akt == 3) {
                akt = 4;
                misto = 3;
                Pribeh.vypisOutroTretihoAktu();
                player.vyberVlastnost();
                Pribeh.vypisIntroCtvrtehoAktu();
            } else if (player.XP >= 100 && akt == 4){
                akt = 5;
                misto = 4;
                Pribeh.vypisOutroCtvrtehoAktu();
                player.vyberVlastnost();
                Pribeh.IntroBossFigtu();
                player.HP = player.maxHP; // vyhealuje postavu před boss fightem
                posledniSouboj();
            }
        }

        public static void nahodnaUdalost() throws InterruptedException {
            int udalost = (int) (Math.random() * nahodneUdalosti.length);
            if (nahodneUdalosti[udalost].equals("Souboj")){
                nahodnySouboj();
            } else if (nahodneUdalosti[udalost].equals("Odpočinek")) {
                odpocinSi();
            } else {
                nakupuj();
            }
        }

        public static void pokracujVceste() throws InterruptedException {
            zkontrolujAkt(); // zkontroluje, zda je potřeba navýšit akt
            if (akt != 5 ){ // zkontorule, zda hra není v posledním aktu (boss fight)
                nahodnaUdalost();
            }
        }

        public static void vypisMenu(){
            vycistiKonzoli();
            vypisNazev(mista[misto]);
            System.out.println("Vyber akci:");
            lajnaLajn(20);
            System.out.println("(1) Pokračuj na své cestě");
            System.out.println("(2) Info postavy");
            System.out.println("(3) Ukonči hru");
        }

        public static void hracZemrel(){
            vycistiKonzoli();
            vypisNazev("Zemřel jsi...");
            vypisNazev("Na cestě jsi získal " + player.XP + "XP. Přístě to půjde lépe.");
            vypisNazev("Doufám, že jsi si mojí hru užil(a).");
            beziJeste = false;
        }

        public static void infoPostavy(){
            vycistiKonzoli();
            vypisNazev("INFO POSTAVY");
            System.out.println(player.name + "\tHP: " + player.HP + "/" + player.maxHP);
            lajnaLajn(20);
            System.out.println("XP: " + player.XP + "\tZlaťáky: " + player.zlataky);
            lajnaLajn(20);
            System.out.println("Počet lektvarů: " + player.potak);
            lajnaLajn(20);

            //printing the chosen traits
            if(player.nAttackkUpgrades > 0){
                System.out.println("Útočné vlastnosti: " + player.attackUpgrades[player.nAttackkUpgrades - 1]);
                lajnaLajn(20);
            }
            if(player.nDefendUpgrades > 0){
                System.out.println("Obranné vlastnosti: " + player.defendUpgrades[player.nDefendUpgrades - 1]);
            }

            cekejNaReakci();
        }

        public static void nakupuj(){
            vycistiKonzoli();
            vypisNazev("Na cestě jsi potkal obchodníka.\nNěco ti nabízí.");
            int cena = (int) (Math.random()* (10 + player.potak*3) + 10 + player.potak);
            System.out.println("Léčící lektvar: " + cena + " zlaťáků.");
            lajnaLajn(20);
            System.out.println("Chceš si jeden koupit?\n(1) Ano\n(2) Ne");
            int input = prectiInt("->", 2);
            if (input == 1){
                vycistiKonzoli();
                if (player.zlataky >= cena){
                    System.out.println("Koupil(a) jsi si lektvar za " + cena + " zlaťáků.");
                    player.potak++;
                    player.zlataky -= cena;
                } else {
                    vypisNazev("Nemáš dostatek zlaťáků.");
                    cekejNaReakci();
                }
            }
        }

    public static int getRandomInteger(int maximum, int minimum){
        return ((int) (Math.random()*(maximum - minimum))) + minimum;
    }

        public static void odpocinSi(){
            vycistiKonzoli();
            if (player.pocetOdpocinku >= 1){
                vypisNazev("Chceš si odpočinout? (Zbývá ti " + player.pocetOdpocinku + " odpočinků).");
                System.out.println("(1) Ano\n(2) Ne");
                int input = prectiInt("->", 2);
                if (input == 1){
                    vycistiKonzoli();
                    if (player.HP < player.maxHP){
                        int RegeneraceHP = getRandomInteger(15,1);
                        player.HP += RegeneraceHP;
                        if (player.HP > player.maxHP){
                            player.HP = player.maxHP;
                        }
                        System.out.println("Odpočinul(a) jsi si a vyléčil(a) jsi si tím " + RegeneraceHP + " HP.");
                        System.out.println("Právě teď máš " + player.HP + "/" + player.maxHP + " HP.");
                        player.pocetOdpocinku--;
                    } else if (player.HP == player.maxHP) {
                        System.out.println("Právě teď si nemůžeš odpočinout, protože máš plné životy.");
                    }
                }
                if (input == 2){
                    System.out.println("Nechceš si odpočinout.");
                }
                cekejNaReakci();
            } else {
                System.out.println("Právě teď nejsi unavený(á).");
            }
        }

        public static void herniLoop() throws InterruptedException {
            while(beziJeste){
                vypisMenu();
                int input = prectiInt("->", 3);
                if(input == 1)
                    pokracujVceste();
                else if(input == 2)
                    infoPostavy();
                else
                    beziJeste = false;
            }
        }

        public static void nahodnySouboj() throws InterruptedException {
            vycistiKonzoli();
            vypisNazev("Na tvé cestě se objevil poskok. Musíš s ním bojovat!");
            cekejNaReakci();
            souboj(new Enemy(enemies[(int)(Math.random()*enemies.length)], player.XP));
        }
        public static void posledniSouboj() throws InterruptedException {
            souboj(new Enemy("Gabriel Payne", 50));
            Pribeh.vypisKonec(player);
            beziJeste = false;
        }

        public static void souboj(Enemy enemy) throws InterruptedException {
        while(true){
            vycistiKonzoli();
            vypisNazev(enemy.name + "\nHP: " + enemy.HP + "/" + enemy.maxHP);
            vypisNazev(player.name + "\nHP: " + player.HP + "/" + player.maxHP);
            System.out.println("Vyber akci:");
            lajnaLajn(20);
            System.out.println("(1) Boj\n(2) Vypij lektvar\n(3) Utéct");
            int input = prectiInt("-> ", 3);
            if(input == 1){
                int dmg = player.attack() - enemy.defend();
                int dmgTook = enemy.attack() - player.defend();
                if(dmgTook < 0){
                    dmg -= dmgTook/2;
                    dmgTook = 0;
                }
                if(dmg < 0)
                    dmg = 0;
                player.HP -= dmgTook;
                enemy.HP -= dmg;
                vycistiKonzoli();
                vypisNazev("SOUBOJ");
                System.out.println("Způsobil(a) jsi " + dmg + " bodů poškození " + enemy.name + ".");
                lajnaLajn(15);
                System.out.println(enemy.name + " ti způsobil " + dmgTook + " bodů poškození.");
                Thread.sleep(2500);
                if(player.HP <= 0){
                    hracZemrel();
                    break;
                }else if(enemy.HP <= 0){
                    vycistiKonzoli();
                    vypisNazev("Porazil(a) jsi " + enemy.name + ".");
                    player.XP += enemy.XP;
                    System.out.println("Získal jsi "+ enemy.XP + " XP.");
                    boolean pridejOdpocinek = (Math.random()*5 + 1 <= 2.25);
                    int pridejZlataky = (int) (Math.random()*enemy.XP);
                    if (pridejOdpocinek){
                        player.pocetOdpocinku++;
                        System.out.println("Zasloužíš si další odpočinek.");
                    }
                    if (pridejZlataky > 0){
                        player.zlataky += pridejZlataky;
                        System.out.println("Získal(a) jsi " + pridejZlataky + " zlaťáků z mrtvoly " + enemy.name);
                    }
                    cekejNaReakci();
                    break;
                }
            }else if(input == 2){ //lektvary
                vycistiKonzoli();
                if (player.potak > 0 && player.HP < player.maxHP){
                    vypisNazev("Chceš vypít lektvar? (Zbývá ti " + player.potak + " lektvar/ů).");
                    System.out.println("(1) Ano\n(2) Ne");
                    input = prectiInt("->", 2);
                    if (input == 1){
                        player.HP = player.maxHP;
                        vycistiKonzoli();
                        vypisNazev("Vypil(a) jsi lektvar léčení. Tvoje zdraví je nyní: " + player.maxHP + " HP.");
                        cekejNaReakci();
                    }
                } else {
                    vypisNazev("Nemáš žádný lektvar, nebo máš plné zdraví.");
                    cekejNaReakci();
                }
            }else{
                vycistiKonzoli();
                if(akt != 5){
                    if(Math.random()*10 + 1 <= 3.5){ //šance 35% na útěk
                        vypisNazev("Utekl(a) jsi před " + enemy.name + ".");
                        cekejNaReakci();
                        break;
                    }else{
                        vypisNazev("Nepovedlo se ti utéct.");
                        int dmgTook = enemy.attack();
                        System.out.println("při útěku jsi obdržel(a) " + dmgTook + " bodů poškození.");
                        player.HP -= dmgTook;
                        cekejNaReakci();
                        if(player.HP <= 0)
                            hracZemrel();
                    }
                }else{
                    vypisNazev("!"); //boss fight
                    cekejNaReakci();
                }

            }
        }
    }
}